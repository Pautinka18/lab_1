<?php

class Users
{
    /**
     * User constructor.
     * @param $name
     * @param $surname
     * @param $role
     * @param $login
     * @param $password
     */
    public function __construct($user)
    {
        $this->name = $user['name'];
        $this->surname = $user['surname'];
        $this->role = $user['role'];
        $this->login = $user['login'];
        $this->password = $user['password'];
    }

}

class Admin extends Users
{
    public function printText()
    {
        echo 'Здравствуйте админ ' . $this->name . ' ' . $this->surname . '. Вы можете на сайте делать всё.';
    }
}

class Client extends Users
{
    public function printText()
    {
        echo 'Здравствуйте клиент ' . $this->name . ' ' . $this->surname . '. Вы можете на сайте просматривать информацию доступную пользователям.';
    }
}

class Manager extends Users
{
    public function printText()
    {
        echo 'Здравствуйте менеджер ' . $this->name . ' ' . $this->surname . '. Вы можете на сайте изменять, удалять и создавать клиентов.';
    }
}

$a = $_GET['names'];
$b = $_GET['second'];
$admin = null;
$arr = [
    [
        'login' => 'Pautinka',
        'password' => 'admin123',
        'role' => 'admin',
        'name' => 'Danil',
        'surname' => 'Kolomoets',
    ],
    [
        'login' => 'bodya213',
        'password' => 'password12345',
        'role' => 'admin',
        'name' => 'Bogdan',
        'surname' => 'Novitskiy',
    ],
    [
        'login' => 'Petya',
        'password' => '123555333',
        'role' => 'manager',
        'name' => 'Petya',
        'surname' => 'Savitskiy',
    ],
    [
        'login' => 'Stepan',
        'password' => 'Nedrya12345',
        'role' => 'client',
        'name' => 'Stepan',
        'surname' => 'Nedrya',
    ]
];
$roles= [
    'admin'=>Admin::class,
    'manager'=>Manager::class,
    'client'=>Client::class,
];
foreach ($arr as $user) {
    if ($a == $user['login'] && $b == $user['password']) {
        $admin = new $roles[$user['role']]($user);
        break;
    }
}
if ($admin) {
    $admin->printText();
} else {
    echo 'Неверный логин и\или пароль.';
}
?>